/*
 * main.c
 */

#ifndef _MAIN_C
#define _MAIN_C
#define FREQUENCY 1000

#include "sample.h"
#include <stdio.h>
#include <stdint.h>

#include "usbstk5505.h"
#include "usbstk5505_gpio.h"
#include "usbstk5505_i2c.h"
#include "usbstk5505_i2s.h"

Int16 dataTable[NUM_POINTS];

#endif

int main(void) {
	Int16 gain = 0; //dB
	uint16_t samplingFrequency = 24000;
	Int16 samplingDuration = 10;


	//
	printf("Enter the desired gain in dB (-6dB to 29dB): \n");
	scanf("%d", &gain);
	if (gain > 29 || gain < -6)
	{
		printf("Gain value out of range. Defaulting to 0dB.\n");
		gain = 0;
	}

	printf("Enter the desired Sampling Frequency in Hz (8, 12, 16, 24, 48 kHz): \n");
	scanf("%d", &samplingFrequency);
	switch(samplingFrequency)
	{
		case 48000:
		case 24000:
		case 16000:
		case 12000:
		case 8000:
			break;
		default:
			printf("Invalid Sampling Frequency, Setting to default of 48kHz\n");
			samplingFrequency = 48000;
			break;
	}

	printf("Enter the desired duration in seconds (5s to 60s): \n");
	scanf("%d", &samplingDuration);
	if ((samplingDuration < 5) || (samplingDuration > 60))
	{
		printf("sampling Duration out of range. Defaulting to 10s.\n");
		samplingDuration = 10;
	}
	
	 uint16_t inputIter = 0;
	 int16_t outputIter = 0;
	 //Generate Sine Wave
	 while(outputIter < NUM_POINTS)
	 {
	 	if (inputIter >= NUM_POINTS)
	   	{
	   		inputIter = 0;
	   	}
	   	dataTable[outputIter] = lookupTable[inputIter];
	   	inputIter += 48000/samplingFrequency;
	   	outputIter++;
	 }

	 tone((Uint32) samplingFrequency, samplingDuration, gain, 0, *dataTable);

	//printf("Wave Parameters are:\nGain = %d\nSampling Frequency = %d\n Sampling Duration = %d\n", gain, samplingFrequency, samplingDuration);

	return 0;
}
