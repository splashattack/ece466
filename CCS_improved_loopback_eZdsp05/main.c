/*****************************************************************************/
/*                                                                           */
/* FILENAME                                                                  */
/* 	 main.c                                                                  */
/*                                                                           */
/* DESCRIPTION                                                               */
/*   TMS320C5505 USB Stick. Application 2. Improved Audio Template.          */
/*   Take microphone input and send to headphones.                           */
/*                                                                           */
/* REVISION                                                                  */
/*   Revision: 1.00	                                                         */
/*   Author  : Richard Sikora                                                */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* HISTORY                                                                   */
/*   Revision: 1.00                                                          */
/*   5th March 2010. Created by Richard Sikora from TMS320C5510 DSK code.    */
/*                                                                           */
/*****************************************************************************/
/*
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "stdio.h"
#include "usbstk5505.h"
#include "aic3204.h"
#include "PLL.h"
#include "stereo.h"
#include "LEDflasher.h"

Int16 left_input;
Int16 right_input;
Int16 left_output;
Int16 right_output;
Int16 mono_input;


#define SAMPLES_PER_SECOND 24000

/* New. Gain as a #define */
/* Use 30 for microphone. Use 0 for line */
#define GAIN_IN_dB 0

unsigned long int i = 0;

/* New. Variable for step */
unsigned int Step = 0; 

unsigned int gain = 0;

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  main( )                                                                 *
 *                                                                          *
 * ------------------------------------------------------------------------ */
void main( void ) 
{
    /* Initialize BSL */
    USBSTK5505_init( );
	
	/* Initialize PLL */
	pll_frequency_setup(100);

    /* Initialise hardware interface and I2C for code */
    aic3204_hardware_init();
    
    /* Initialise the AIC3204 codec */
	aic3204_init(); 

    printf("\n\nRunning Improved Audio Template Project\n");
    printf( "<-> Audio Loopback from Stereo IN --> to HP/Lineout\n\n" );

	/* Setup sampling frequency and 30dB gain for microphone */
    set_sampling_frequency_and_gain(SAMPLES_PER_SECOND, GAIN_IN_dB);

    left_input *= 2;
    right_input /= 2;
	/* New. Add descriptive text */ 
    puts("\nChanges configuration once every 20 seconds and flashes LED");
    puts(" 1 Flash   = Straight through, no signal processing");
    puts(" 2 Flashes = Convert stereo to mono");
    puts(" 3 Flashes = Left input -> right output. Right input -> left output");	
    
	/* New. Default to XF LED off */
	asm(" bclr XF");
   
 	for ( i = 0  ; i < SAMPLES_PER_SECOND * 600L  ;i++  )
 	{

     aic3204_codec_read(&left_input, &right_input); // Configured for one interrupt per two channels.

     mono_input = stereo_to_mono(left_input, right_input);
	 
	 /* New. LED flasher */
      Step = LEDFlasher(3); // Takes total number of steps as argument, here 3.

      /* Fade in effect */
      if ((i % (SAMPLES_PER_SECOND/5)) == 0)
      {
    	  if (gain == 10)
    		  gain = 0;
    	  else
    		  gain++;
    	  set_sampling_frequency_and_gain(SAMPLES_PER_SECOND, gain);
      }
   

	  if ( Step == 1 )
        {
		 left_output = left_input;
		 right_output = right_input;
	    } 
	  else if ( Step == 2)
        {	  
          left_output =  mono_input;    // Stereo to mono
          right_output = mono_input;   
        } 
      else if ( Step == 3)
        {
		  left_output = right_input;    // Swap left and right channels
		  right_output = left_input;
		}  	  
     aic3204_codec_write(left_output, right_output);
 	}

   /* Disable I2S and put codec into reset */ 
    aic3204_disable();

    printf( "\n***Program has Terminated***\n" );
    SW_BREAKPOINT;
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  End of main.c                                                           *
 *                                                                          *
 * ------------------------------------------------------------------------ */
