/*
 * main.c
 * Created by: Jose Sanchez
 * 			(modifird from RTDSP Fundamentls by Kuo et al.)
 * 			on Januart 15, 2014
 *
 * 	Objective: The goal of this program is to read a DAT file
 * 	 and write a DAT file using breakpoints. DSP
 * 	 processing can be placed in between
 */
#pragma DATA_SECTION(outBuffer, "expdata0");
#pragma DATA_SECTION(inBuffer, "expdata1");

#define DATALENGTH 17

short inBuffer[DATALENGTH];
short outBuffer[DATALENGTH];

void main()
{
	short i;
	for (i=0; i<DATALENGTH; i++) // <- breakpoint read
	{
		outBuffer[i] = inBuffer[i]*inBuffer[i];
	}							// <- breakpoint write
}
