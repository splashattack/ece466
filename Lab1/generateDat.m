% generateDat.m

MAGIC_NUMBER = 1651;
HEX_FORMAT = 1;
INT_FORMAT = 2;
LONG_FORMAT = 3;
FLOAT_FORMAT = 3;
STARTING_ADDRESS = 0;
PAGE_NUMBER = 0;
LENGTH = 17;
DATA_BLOCK = linspace(0,16,LENGTH);


outFile = fopen('dataIn.dat','w+');
str = [MAGIC_NUMBER,INT_FORMAT,STARTING_ADDRESS,PAGE_NUMBER,LENGTH];
fprintf(outFile,'%u %u %u %u %u\n', str);
for i = 1:LENGTH
   fprintf(outFile,'%u\n',DATA_BLOCK(i));
end
fclose(outFile);