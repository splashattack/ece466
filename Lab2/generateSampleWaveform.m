function [ ] = generateSampleWaveform()
%generateSampleWaveform - Generates a sample waveform lookup table and outputs in the
%form of a C include file

b = 16; %Number of bits used in precision
N = 48; %Total numberof points

outFile = fopen('sample.h','w+');
fprintf(outFile, '//sample.h\n');
fprintf(outFile, '//Auto-generated file that includes a sample waveform lookup table\n');
fprintf(outFile, '#ifndef _SAMPLE_H\n');
fprintf(outFile, '#define _SAMPLE_H\n\n');
fprintf(outFile, '#include <stdint.h>\n\n');
fprintf(outFile, '#endif\n\n');
fprintf(outFile, 'static const int%u_t dataTable[%u] =\n',b, N);
fprintf(outFile, '{\n');
for n = 0:N-1
    fprintf(outFile, '    %d,\n', floor(((2^b)-1)/2*sin(2*pi*n/N)));
end
fprintf(outFile, '};\n');
fclose(outFile);