/*
 * main.c
 */

#ifndef _MAIN_C
#define _MAIN_C
#define FREQUENCY 1000

#include "sample.h"
#include <stdio.h>
#include <stdint.h>

int16_t dataTable[NUM_POINTS];

#endif

int main(void) {
	int16_t gain = 0; //dB
	uint32_t samplingFrequency = 24000;
	int16_t samplingDuration = 10;


	//
	printf("Enter the desired gain in dB (-6dB to 29dB): \n");
	scanf("%d", &gain);
	if (gain > 29 || gain < -6)
	{
		printf("Gain value out of range. Defaulting to 0dB.\n");
		gain = 0;
	}

	printf("Enter the desired Sampling Frequency in Hz (8, 12, 16, 24, 48 kHz): \n");
	scanf("%d", &samplingFrequency);
	if ((samplingFrequency != (uint16_t)48000) &&
		(samplingFrequency != (uint16_t)24000) &&
		(samplingFrequency != (uint16_t)16000) &&
		(samplingFrequency != (uint16_t)12000) &&
		(samplingFrequency != (uint16_t)8000))
		{
			printf("Invalid Sampling Frequency, Setting to default of 48kHz\n");
			samplingFrequency = 48000;
		}

	printf("Enter the desired duration in seconds (5s to 60s): \n");
	scanf("%d", &samplingDuration);
	if ((samplingDuration < 5) || (samplingDuration > 60))
	{
		printf("sampling Duration out of range. Defaulting to 10s.\n");
		samplingDuration = 10;
	}
	
	 uint16_t inputIter = 0;
	 int16_t outputIter = 0;
	 //Generate Sine Wave
	 while(outputIter < NUM_POINTS)
	 {
	 	if (inputIter >= NUM_POINTS)
	   	{
	   		inputIter = 0;
	   	}
	   	dataTable[outputIter] = lookupTable[inputIter];
	   	inputIter += 48000/samplingFrequency;
	   	outputIter++;
	 }

	tone(samplingFrequency, samplingDuration, gain, 0, *dataTable);

	//printf("Wave Parameters are:\nGain = %d\nSampling Frequency = %d\n Sampling Duration = %d\n", gain, samplingFrequency, samplingDuration);

	return 0;
}
