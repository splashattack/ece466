//sample.h
//Auto-generated file that includes a sample waveform lookup table
#ifndef _SAMPLE_H
#define _SAMPLE_H

#include <stdint.h>

#define NUM_POINTS 48

#endif

static const int16_t lookupTable[NUM_POINTS] =
{
    0,
    4277,
    8480,
    12539,
    16383,
    19947,
    23170,
    25996,
    28377,
    30273,
    31650,
    32487,
    32767,
    32487,
    31650,
    30273,
    28377,
    25996,
    23170,
    19947,
    16383,
    12539,
    8480,
    4277,
    0,
    -4278,
    -8481,
    -12540,
    -16384,
    -19948,
    -23171,
    -25997,
    -28378,
    -30274,
    -31651,
    -32488,
    -32768,
    -32488,
    -31651,
    -30274,
    -28378,
    -25997,
    -23171,
    -19948,
    -16384,
    -12540,
    -8481,
    -4278,
};
